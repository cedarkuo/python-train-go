from selenium.webdriver import ChromeOptions, Chrome

class Handler:
    def __init__(self, info_json):
        self.info_dict = info_json
        self.go_date = info_json["getin_date"]

    def get_go_date(self, driver):
        elements = driver.find_element_by_id("getin_date")
        date_elements = elements.find_elements_by_tag_name("option")
        for date in date_elements:
            raw_date = date.get_attribute("value")
            process_date = raw_date.split("-")
            if process_date[0] == self.go_date:
                return raw_date

    def run(self):
        option = ChromeOptions()
        option.add_experimental_option("detach", True)
        driver = Chrome('C:\chromedriver', chrome_options=option)
        # http://railway.hinet.net/Foreign/TW/etno1.html 單程票
        # http://railway.hinet.net/Foreign/TW/etno_roundtrip.html 去回票
        driver.get("http://railway.hinet.net/Foreign/TW/etno1.html")

        try:
            driver.switch_to_window(driver.window_handles[0])
            driver.find_element_by_id("person_id").send_keys(self.info_dict["person_id"])
            driver.find_element_by_id("from_station").send_keys(self.info_dict["from_station"])
            driver.find_element_by_id("to_station").send_keys(self.info_dict["to_station"])
            driver.find_element_by_id("getin_date").send_keys(self.get_go_date(driver))
            driver.find_element_by_id("train_no").send_keys(self.info_dict["train_no"])
            driver.find_element_by_id("order_qty_str").send_keys(self.info_dict["order_qty_str"])
            driver.find_element_by_xpath("//*[@class='btn btn-primary']").click()
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            driver.find_element_by_id("randInput").click()
        except Exception as e:
            print(e)
            # driver.close()