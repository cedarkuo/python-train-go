import os
import simplejson as json

from train.handler import Handler


def get_info():
    workspace = os.getcwd()
    with open("{}/info.json".format(workspace)) as f:
        result = json.load(f)
    return result

def main():
    train = Handler(get_info())
    train.run()


if __name__ == "__main__":
    main()
