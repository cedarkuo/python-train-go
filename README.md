How to use go-train-go
===

切換到 go-train-go 資料夾裡
修改 info.json 檔案，如果不清楚每個欄位代表的意義，可以參考 info_example.json

改完之後就可以跑 script，use virtualenv on linux

如果不是用 pipenv 的話這邊有兩種跑的方法
1. 進入 venv 環境後在執行
```
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
python main.py
```

2. 不用進入 venv 環境直接執行，但還是使用 venv 的環境（也就是不用 activate）
```
venv/bin/pip install -r requirements.txt
venv/bin/python main.py
```
